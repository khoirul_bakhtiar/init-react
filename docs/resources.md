## Change React Route v5 to v6

# Link :

https://typescript.tv/react/upgrade-to-react-router-v6/
https://stackoverflow.com/questions/70059188/use-urlparams-in-mapstatetoprops-in-react-router-v6
https://reactrouter.com/docs/en/v6/getting-started/overview

## Create root from React v17 to v18

# Link :

https://reactjs.org/blog/2022/03/08/react-18-upgrade-guide.html#updates-to-client-rendering-apis

## New Features on React 18

# Link :

https://www.androprojek.my.id/2021/11/fitur-fitur-yang-akan-hadir-di-react-18.html

## Issue migration react-router v6

# Connected React Router

# Link Github :

https://github.com/supasate/connected-react-router

# Link Issue :

https://stackoverflow.com/questions/71122804/use-of-react-router-v6-history-navigate-in-redux
