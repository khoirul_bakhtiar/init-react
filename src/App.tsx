import React from 'react';
import { Provider } from 'react-redux';

import { GlobalStyles } from 'constant';
import Router from 'router';
import { history, store } from 'store';

const App = () => {
	return (
		<Provider store={ store }>
			<GlobalStyles/>
			<Router />
		</Provider>
	);
};
export default App;
