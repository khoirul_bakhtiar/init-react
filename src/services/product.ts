import { Common } from 'interfaces';
import { Endpoints } from 'constant';
import { customFetch } from 'helpers';

import * as setData from './product.setData';

export const getProduct = async(params: Common.ApiParams) => {
  try {
    const query = params
    ? `?page=${params.page ? params.page : ''}&keyword=${params.keyword ? params.keyword : ''}&sort=desc`
    : '';

    const apiFetch = await customFetch(`${Endpoints.baseUrl}${Endpoints.param.product}${query}`, 'GET');

    const response = await apiFetch?.json();

    if (response.data) {
      //
      response.data = setData.setProduct(response.data);
    } else {
      response.data = {};
    }
    
    return response;
  } catch (error) {
    throw error;
  }
};
