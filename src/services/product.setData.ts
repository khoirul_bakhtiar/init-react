import { Product } from 'interfaces';

export const setProduct = (product: Product.APIDetail) => ({
  id: String(product.id),
  name: product.product_name
});
