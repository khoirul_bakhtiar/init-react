import { Product } from './index';

export interface ApiResponseData<T> {
  stat_code: number;
  stat_msg: string;
  pagination: Pagination;
  data: T;
}

export interface ApiParams {
  id?: string;
  page?: string;
  keyword?: string;
}

export interface Pagination {
  count?: number;
  keyword?: string;
  limit?: number;
  offset?: number;
  order?: string;
  page?: number;
  sort?: string;
}

export interface RootState {
  product: Product.State
}
