/* eslint-disable react/jsx-no-bind */
import React from 'react';
import {
	Routes,
	Route,
	Navigate,
} from 'react-router-dom';

import { LocalStorage } from 'helpers';

const PrivateRoute = ({
	children
}: { children: JSX.Element}) => {
	const token = LocalStorage.getToken();
	return (
	  <React.Fragment>
			{
		  token ?
			 children
					: <Navigate to='/login' />
			}
	  </React.Fragment>
	);
};

export default PrivateRoute;
