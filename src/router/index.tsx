/* eslint-disable no-console */
import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

import { Dashboard } from 'screens';
import { GlobalStyles } from 'constant';
import PrivateRoute from './private';

const publicRoutes = [
	{
		path: '/',
		component: Dashboard,
	}, {
		path: '/404',
		component: Dashboard,
	},
];

const Router = () => {
		
	return (
		<BrowserRouter>
			<GlobalStyles />
			<Routes>
				<Route path='/*' element={ <PrivateRoute>
					<Routes>
						{
							publicRoutes.map(route =>
								<Route key={ route.path } path={ route.path } element={ route.component } />
							)
						}
					</Routes>
				</PrivateRoute> } />
				
			</Routes>
		</BrowserRouter>
	);
};

export default Router;
