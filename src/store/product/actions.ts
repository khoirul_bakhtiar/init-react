import { Common, Product } from 'interfaces';
import * as productServices from 'services/product';

import { ProductActionTypes } from './actionTypes';

export const getProductLoading = () => ({
  type: ProductActionTypes.GET_PRODUCT_LIST,
});

export const getProductSuccess = (payload: {data: Product.Detail[], pagination: Common.Pagination}) => ({
  type: ProductActionTypes.GET_PRODUCT_LIST_SUCCESS,
  payload,
});

export const getProduct = (dispatch: any) => (params:Common.ApiParams) => {
  return new Promise<void>(async(resolve, reject) => {
    dispatch(getProductLoading());

    const response: Common.ApiResponseData<Product.Detail[]> = await productServices.getProduct(params);

    const { stat_code, data, stat_msg, pagination } = response;

    if (stat_code === 200) {
      resolve(dispatch(getProductSuccess({ data, pagination })));
    } else {
      reject(stat_msg);
    }
  });
};
