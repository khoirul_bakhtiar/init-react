import { Product } from 'interfaces';

import { ProductActionTypes } from './actionTypes';
import * as actions from './actions';

const initialState: Product.State = {
	list: [],
	loadingGet: false
};

const reqProduct = (state: Product.State) => ({
	...state,
	loadingGet: true
});

const setProduct = (state: Product.State, payload) => ({
	...state,
	list: payload.data,
	loadingGet: false
});

const productReducer = (state = initialState, action: {type: any; payload: any;}) => {
	switch (action.type) {
		case ProductActionTypes.GET_PRODUCT_LIST:
			return reqProduct(state);
		case ProductActionTypes.GET_PRODUCT_LIST_SUCCESS:
			return setProduct(state, action.payload);
		default:
			return state;
	}
};

export default productReducer;
