import { compose, applyMiddleware, createStore, combineReducers } from 'redux';
import { routerMiddleware, connectRouter } from 'connected-react-router';
import thunk from 'redux-thunk';

import { History as history } from 'helpers';

import productReducer from './product/reducers';
const routeMiddleware = routerMiddleware(history);

const rootReducer = combineReducers({
	product: productReducer,
	router: connectRouter(history),

});

const store = createStore(rootReducer, compose(applyMiddleware(thunk, routeMiddleware)));

export { history, store };
